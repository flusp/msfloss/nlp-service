# nlp-service

## Functional Tests

Go back into the main directory and ramp up the nlp-service server:

```
cd ..
docker-compose up
```

Install all pipenv dependencies and switch into the pipenv shell:

```
pipenv install
pipenv shell
```

Then just run the tests:

```
tavern-ci tests
```
