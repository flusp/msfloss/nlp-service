#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest
import json
from sqlalchemy import create_engine
import os

@pytest.fixture
def reset_data():
    try:
        db_uri = os.environ['DB_URI']
    except KeyError:
        db_uri = 'postgresql://development_user:pass@localhost:5432/development_db'
    engine = create_engine(db_uri)
    if engine.has_table('events'):
        engine.execute("delete from events")
    if engine.has_table('conversations'):
        engine.execute("delete from conversations")
    if engine.has_table('users'):
        engine.execute("delete from users")

def validate_event_sentiment(response,event_position=None,score_type=None,check=None):
    check_expr = eval(check)
    json_response = json.loads(response.text)
    score = json_response["events"][event_position]["scores"][score_type]
    if not check_expr(score):
        raise Exception("Event sentiment score of type \"%s\" at position [%d] with value \"%s\" does not match condition: \"%s\""%(score_type,event_position,score,check))
    pass

def validate_array_length(response,length=None,key_name=None):
    json_response = json.loads(response.text)
    array = json_response[key_name]
    if len(array) != length:
        raise Exception("Length of array '%s' with %d elements does not match expected value of %d elemets"%(key_name,len(array),length))
    pass
