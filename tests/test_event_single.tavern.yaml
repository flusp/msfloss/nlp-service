test_name: Test insertion of a single event

includes:
  - !include parameters.yaml

marks:
  - usefixtures:
    - reset_data

stages:
  - name: Insert a conversation to get stared with
    request:
      url: "{base_url:s}/{conversation_route:s}/"
      method: POST
      headers:
        content-type: application/json
      json:
        source: IRC
        context:
          server: testserver.com
          channel: some_channel
    response:
      status_code: 201
      body:
        uuid: !anystr
      save:
        body:
          conversation_id: uuid

  - name: Insert an event
    request:
      url: "{base_url:s}/{event_route:s}/{conversation_id:s}"
      method: POST
      headers:
        content-type: application/json
      json:
        user: Some Test User
        type: MSG
        timestamp: "2019-06-05"
        content: "Some very good message content"
    response:
      status_code: 201
      body:
        id: !anyint
      save:
        body:
          event_id: id

  - name: Retrieve the event
    request:
      url: "{base_url:s}/{event_route:s}/{conversation_id:s}/{event_id:d}"
      method: GET
    response:
      status_code: 200
      body:
        id: !int "{event_id:d}"
        type: MSG
        timestamp: "2019-06-05T00:00:00"
        content: "Some very good message content"
        conversation: "{conversation_id:s}"
        user: Some Test User

  - name: Insert an event without timestamp
    request:
      url: "{base_url:s}/{event_route:s}/{conversation_id:s}"
      method: POST
      headers:
        content-type: application/json
      json:
        user: Some Test User
        type: MSG
        content: "Some very good message content"
    response:
      status_code: 201
      body:
        id: !anyint
      save:
        body:
          event_id: id

  - name: Retrieve the event without timestamp
    request:
      url: "{base_url:s}/{event_route:s}/{conversation_id:s}/{event_id:d}"
      method: GET
    response:
      status_code: 200
      body:
        id: !int "{event_id:d}"
        type: MSG
        timestamp: !anystr
        content: "Some very good message content"
        conversation: !anystr
        user: Some Test User

  - name: Insert an event with datetime timestamp
    request:
      url: "{base_url:s}/{event_route:s}/{conversation_id:s}"
      method: POST
      headers:
        content-type: application/json
      json:
        user: Some Test User
        type: MSG
        timestamp: "2019-06-20T20:42:30"
        content: "Some very good message content"
    response:
      status_code: 201
      body:
        id: !anyint
      save:
        body:
          event_id: id

  - name: Retrieve the event datetime timestamp
    request:
      url: "{base_url:s}/{event_route:s}/{conversation_id:s}/{event_id:d}"
      method: GET
    response:
      status_code: 200
      body:
        id: !int "{event_id:d}"
        type: MSG
        timestamp: "2019-06-20T20:42:30"
        content: "Some very good message content"
        conversation: !anystr
        user: Some Test User

  - name: Insert an event without type should return an error message
    request:
      url: "{base_url:s}/{event_route:s}/{conversation_id:s}"
      method: POST
      headers:
        content-type: application/json
      json:
        user: Some Test User
        timestamp: "2019-06-05"
        content: "Some very good message content"
    response:
      status_code: 400
      body:
        message: Input payload validation failed
        errors:
          type: "'type' is a required property"

  - name: Insert an event with an invalid type should return an error message
    request:
      url: "{base_url:s}/{event_route:s}/{conversation_id:s}"
      method: POST
      headers:
        content-type: application/json
      json:
        user: Some Test User
        type: XPTO
        timestamp: "2019-06-05"
        content: "Some very good message content"
    response:
      status_code: 400
      body:
        message: Input payload validation failed
        errors:
          type: !anystr

  - name: Insert an event without user should return an error message
    request:
      url: "{base_url:s}/{event_route:s}/{conversation_id:s}"
      method: POST
      headers:
        content-type: application/json
      json:
        type: MSG
        timestamp: "2019-06-05"
        content: "Some very good message content"
    response:
      status_code: 400
      body:
        message: Input payload validation failed
        errors:
          user: "'user' is a required property"

  - name: Insert an event without content should return an error message
    request:
      url: "{base_url:s}/{event_route:s}/{conversation_id:s}"
      method: POST
      headers:
        content-type: application/json
      json:
        user: Some Test User
        type: MSG
        timestamp: "2019-06-05"
    response:
      status_code: 400
      body:
        message: Input payload validation failed
        errors:
          content: "'content' is a required property"

  - name: Insert an event providing an invalid uuid conversation_id in the route should return an error message
    request:
      url: "{base_url:s}/{event_route:s}/some_random_conversation_id"
      method: POST
      headers:
        content-type: application/json
      json:
        user: Some Test User
        type: MSG
        timestamp: "2019-06-05"
        content: "Some very good message content"
    response:
      status_code: 400
      body:
        message: Invalid Route
        errors:
          conversation_id: "Invalid uuid format for conversation id 'some_random_conversation_id'"

  - name: Insert an event providing a non existing uuid conversation_id in the route should return an error message
    request:
      url: "{base_url:s}/{event_route:s}/6a1c62c6-0c6a-48b3-bed0-624443d91461"
      method: POST
      headers:
        content-type: application/json
      json:
        user: Some Test User
        type: MSG
        timestamp: "2019-06-05"
        content: "Some very good message content"
    response:
      status_code: 404
      body:
        message: Invalid Route
        errors:
          conversation_id: "Could not find any conversation with id '6a1c62c6-0c6a-48b3-bed0-624443d91461'"

  - name: Insert an event without providing the conversation_id in the route should return an error message
    request:
      url: "{base_url:s}/{event_route:s}/"
      method: POST
      headers:
        content-type: application/json
      json:
        user: Some Test User
        type: MSG
        timestamp: "2019-06-05"
        content: "Some very good message content"
    response:
      status_code: 405
      body:
        message: The method is not allowed for the requested URL.
