# nlp-service

## Development Environment

Just run

```
docker-compose up
```

and the development environment will be set up at port 5000.
Auto-reloading is activated.

## Production Environment

Container must be started setting the following environment values

 - DB_URI: URI of the database to be used
 - FLASK_ENV: Environment of the Flask application (Must be production)
 - PORT: Only if you're not happy with the default port 5000

 ## Documentation

 For a complete description of the API, please refer to our [Swagger documentation](https://flusp.gitlab.io/msfloss/nlp-service).

 More details about the nlp service and project itself [can be found on the Wiki page](https://gitlab.com/flusp/msfloss/nlp-service/wikis/home).
