
# Base docker image (With required libs)
FROM python:3.7 as BASE

ENV PORT=5000
ENV FLASK_APP=/app/src/nlp_service.py
ENV PYTHONPATH=.:/usr/local/bin/python

EXPOSE ${PORT}

WORKDIR /app
COPY requirements.txt .
RUN pip install --trusted-host pypi.python.org -r requirements.txt && rm -r requirements.txt


# Development environment
FROM BASE as development

ENV FLASK_ENV=development

CMD flask run --host=0.0.0.0 --port ${PORT}

# Production environment
FROM BASE as production

COPY src ./src/

ENV FLASK_ENV=production

CMD flask run --host=0.0.0.0 --port ${PORT}
