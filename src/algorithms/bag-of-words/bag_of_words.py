from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
import pandas as pd

class BagOfWords:
    """
    A bag-of-words model for a list of strings. 
        
    Example Usage:
        words = BagOfWords()
        words.documents = ["Arthur likes to play videogames. Anna does not like videogames.", 
        "Carl does not play the piano."]
        print(words)
    """

    count_vectorizer = CountVectorizer()
    tfidf_vectorizer = TfidfVectorizer()

    def __init__(self, vectorizer = None):
        """
        Args:
            vectorizer: An optional argument that is an instance of a sklearn vectorizer. 
            Examples are BagOfWords.count_vectorizer and BagOfWords.tfidf_vectorizer.
        """
        
        self.vectorizer = vectorizer if vectorizer is not None else BagOfWords.tfidf_vectorizer

    def __repr__(self):
        return "BagOfWords()"

    def __str__(self):
        if self._model is not None:
            return str(pd.DataFrame(self._model.toarray(), columns = self._features))

        raise Exception("Cannot cast to string an empty BagOfWords")

    @property
    def model(self):
        """Gets the generated model"""

        return self._model

    @property
    def features(self):
        """Gets the features of the bag-of-words model"""
        return self._features

    @property
    def documents(self):
        """Gets or sets the raw documents to be processed"""
        return self._documents

    @documents.setter
    def documents(self, documents):
        self._documents = documents
        self._model = self.vectorizer.fit_transform(documents)

        self._features = self.vectorizer.get_feature_names()
