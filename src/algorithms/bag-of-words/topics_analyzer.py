from sklearn.decomposition import NMF, LatentDirichletAllocation
from bag_of_words import BagOfWords

class TopicsAnalyzer:
    """
    A static class to analyze a bag-of-words model to get its topics. 

    Example Usage:
        text = "Today is the best day of my life. I finally got married to the person I love. The party after the ceremony was awesome too."
        BOW = BagOfWords()
        BOW.documents = [text]
        topics = TopicsAnalyzer.get_topics(BOW, n_topics = 30)
        for index, topic in enumerate(topics):
            message = "Topic %d: " % index
            message += " ".join(BOW.features[i] for i in topic.argsort()[0:2])
            print(message)
    """

    nmf, lda = 0, 1
    
    @staticmethod
    def get_topics(bag_of_words, algorithm = None, n_topics = 5):
        if not isinstance(bag_of_words, BagOfWords):
            raise Exception("\"bag_of_words\" must be a BagOfWords model")
        
        try:
            if algorithm is None or algorithm == TopicsAnalyzer.nmf:
                algorithm = NMF(n_components = n_topics)
            elif algorithm == TopicsAnalyzer.lda:
                algorithm = LatentDirichletAllocation(n_components = n_topics)

            # Fits the model using the selected algorithm
            return algorithm.fit(bag_of_words.model).components_
        except:
            raise Exception("Algorithm failed to be executed")
