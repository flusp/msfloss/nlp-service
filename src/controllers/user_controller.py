#!/usr/bin/env python
# -*- coding: utf-8 -*-

from models.user import User
from flask_app import api
from flask_restplus import Resource, fields
from util.alchemy import db


nm_user = api.namespace(
    "user", description="User data")

user_api_model = api.model('user', {
    'id': fields.String(readonly=True,description="User UUID",example="dc5f1f51-557c-42a0-983c-aa8f1e01efdb"),
    'name': fields.String(description="The name/identifier of the user",example="Joe Doe <joe@server.com>"),
})

user_api_model_list = api.model('User List', {
    'users': fields.List(fields.Nested(user_api_model))
})


@nm_user.route('/')
class UserController(Resource):
    @nm_user.marshal_with(user_api_model_list,code=200)
    def get(self):
        return {"users": list(map(lambda x: x.to_dict(), db.session.query(User).all()))}, 200
