#!/usr/bin/env python
# -*- coding: utf-8 -*-

from models.event import Event
from models.email_chain_filtered_event import EmailChainFilteredEvent
from models.natural_language_filtered_event import NaturalLanguageFilteredEvent
from models.source_code_filtered_event import SourceCodeFilteredEvent
from models.analyzed_event import AnalyzedEvent
from flask_app import api
from flask_restplus import Resource, fields
from util.alchemy import db
from controllers.event_controller import event_api_model

nm_data = api.namespace(
    "data", description='Data processing API'
)


sentiment_scores = api.model('Sentiment Analyer Scores', {
    'neg': fields.Float(description="Ratio of NEGATIVE words",example=0.03),
    'neu': fields.Float(description="Ratio of NEUTRAKL words",example=0.6),
    'pos': fields.Float(description="Ratio of POSITIVE words",example=0.13),
    'compound': fields.Float(description="Compound score, ranging from -1 to +1",example=0.13),
})


event_api_model_with_sentiment = api.inherit('Event with Sentiment Analysis', event_api_model, {
    'scores': fields.Nested(sentiment_scores)
} )


event_api_model_with_sentiment_list = api.model('Event with Sentiment Analysis List', {
    'events': fields.List(fields.Nested(event_api_model_with_sentiment))
})


def decorated_event_dict(event):
    return AnalyzedEvent(SourceCodeFilteredEvent(EmailChainFilteredEvent(event))).to_dict()


@nm_data.route('/sentiment')
class SentimentRoute(Resource):
    @nm_data.marshal_with(event_api_model_with_sentiment_list,code=200)
    def get(self):
        return {"events": list(map(decorated_event_dict, db.session.query(Event).order_by('id').all()))}, 200


@nm_data.route('/sentiment/<string:conversation_id>')
class SentimentRouteWithSentiment(Resource):
    @nm_data.marshal_with(event_api_model_with_sentiment_list,code=200)
    def get(self, conversation_id):
        return {"events": list(map(decorated_event_dict, db.session.query(Event).filter_by(conversation=conversation_id).order_by('id').all()))}, 200
