#!/usr/bin/env python
# -*- coding: utf-8 -*-

from models.dynamic_decorator import DynamicDecorator


class SourceCodeFilteredEvent(DynamicDecorator):

    def __init__(self,event,percentage=0.95):
        super().__init__(event)
        self._percentage = percentage

    def filter_lines(self,content):
        output = []

        for line in content.splitlines(True):
            qtd_alpha = list(map(lambda x: x.isalpha(),line)).count(True)
            qtd_space = list(map(lambda x: x.isspace(),line)).count(True)
            length = len(line)
            if length > 1 and (qtd_alpha+qtd_space)/length > self._percentage:
                output.append(line)

        return ''.join(output)

    @property
    def content(self):
        return self.filter_lines(self._wrappee.content)

    def to_dict(self):
        dict = self._wrappee.to_dict()
        dict['content']  = self.content
        return dict
