#!/usr/bin/env python
# -*- coding: utf-8 -*-
from models.dynamic_decorator import DynamicDecorator
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer

class AnalyzedEvent(DynamicDecorator):

    def __init__(self,event,sentiment_analyzer=SentimentIntensityAnalyzer()):
        super().__init__(event)
        self._sentiment_analyzer = sentiment_analyzer

    @property
    def scores(self):
        return self._sentiment_analyzer.polarity_scores(self.content)

    def to_dict(self):
        dict = self._wrappee.to_dict()
        dict['scores'] = self.scores
        return dict
