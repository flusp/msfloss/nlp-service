#!/usr/bin/env python
# -*- coding: utf-8 -*-

class StubEvent:

    def __init__(self,content=None):
        self.content = content

    def to_dict(self):
        return {
            'content': self.content
        }
