#!/usr/bin/env python
# -*- coding: utf-8 -*-
from models.dynamic_decorator import DynamicDecorator
import unittest


class StubClass:
    def __init__(self,value_a=None,value_b=None):
        self.value_a = value_a
        self.value_b = value_b


    def dummy_read_method(self):
        return self.value_a

    def dummy_write_method(self,value):
        self.value_b = value


class SubDynamicDecorator(DynamicDecorator):

    def __init__(self,obj,public_param=None,protected_param=None,private_param=None):
        super().__init__(obj)
        self.public_param = public_param
        self._protected_param = protected_param
        self.__private_param = private_param

    def set(self,public_param,protected_param,private_param):
        self.public_param = public_param
        self._protected_param = protected_param
        self.__private_param = private_param

    def get(self):
        return [self.public_param,self._protected_param,self.__private_param]


class TestDynamicDecorator(unittest.TestCase):

    def test_read_attributes(self):
        object = StubClass(value_a="Some value a",value_b=421)
        decorated_object = DynamicDecorator(object)
        decorated_object.decorated_value = "Some decorated value"

        self.assertEqual(decorated_object.value_a,"Some value a")
        self.assertEqual(decorated_object.value_b,421)
        self.assertEqual(decorated_object.decorated_value,"Some decorated value")


    def test_write_attributes(self):
        object = StubClass(value_a="Some value a",value_b=421)
        decorated_object = DynamicDecorator(object)
        decorated_object.value_a = "Another value a"
        decorated_object.value_b = 124

        self.assertEqual(object.value_a,"Another value a")
        self.assertEqual(object.value_b,124)


    def test_read_methods(self):
        object = StubClass(value_a="Some value a",value_b=421)
        decorated_object = DynamicDecorator(object)

        self.assertEqual(decorated_object.dummy_read_method(),"Some value a")

    def test_dummy_write_method(self):
        object = StubClass(value_a="Some value a",value_b=421)
        decorated_object = DynamicDecorator(object)
        decorated_object.dummy_write_method(345)

        self.assertEqual(object.value_b,345)

    def test_subclassing_init(self):
        object = StubClass(value_a="Some value a",value_b=421)
        decorated_object = SubDynamicDecorator(object,public_param="public",protected_param="protected",private_param="private")

        self.assertEqual(decorated_object.get(),["public","protected","private"])

    def test_subclassing_overwrite_public_parameter(self):
        object = StubClass(value_a="Some value a",value_b=421)
        decorated_object = SubDynamicDecorator(object,public_param="public",protected_param="protected",private_param="private")
        decorated_object.public_parameter = "another public parameter"

        self.assertEqual(decorated_object.public_parameter, "another public parameter")

    def test_subclassing_overwrite_via_method(self):
        object = StubClass(value_a="Some value a",value_b=421)
        decorated_object = SubDynamicDecorator(object,public_param="public",protected_param="protected",private_param="private")
        decorated_object.set("another public parameter","another protected parameter","another private parameter")

        self.assertEqual(decorated_object.get(),
                         ["another public parameter","another protected parameter","another private parameter"])

if __name__ == '__main__':
    unittest.main()
