#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
from models.email_chain_filtered_event import EmailChainFilteredEvent
from stub_event import StubEvent


class TestEmailChainFilteredEvent(unittest.TestCase):

    def test_filter_lines_short(self):
        event = StubEvent()
        filtered_event = EmailChainFilteredEvent(event)

        content = "One line\n > a email chain line"

        self.assertEqual(
            filtered_event.filter_lines(content),
            "One line\n"
            )


    def test_filter_lines_long(self):
        event = StubEvent()
        filtered_event = EmailChainFilteredEvent(event)

        content = "One line\n > a email chain line\nOne line\n > a email chain line\nOne line\n > a email chain line\n"

        self.assertEqual(
            filtered_event.filter_lines(content),
            "One line\nOne line\nOne line\n"
            )

    def test_filter_lines_accent(self):
        event = StubEvent()
        filtered_event = EmailChainFilteredEvent(event)

        content = u"One line with ácênt\n > a email chain line with ácênt\n"

        self.assertEqual(
            filtered_event.filter_lines(content),
            u"One line with ácênt\n"
            )

    def test_filter_empty(self):
        event = StubEvent()
        filtered_event = EmailChainFilteredEvent(event)

        content = ""

        self.assertEqual(
            filtered_event.filter_lines(content),
            ""
            )

    def test_filter_none(self):
        event = StubEvent()
        filtered_event = EmailChainFilteredEvent(event)

        content = None

        self.assertEqual(
            filtered_event.filter_lines(content),
            None
            )

    def test_content_decorater(self):
        event = StubEvent("One line\n > a email chain line")
        filtered_event = EmailChainFilteredEvent(event)

        self.assertEqual(
            filtered_event.content,
            "One line\n"
            )


    def test_content_setting(self):
        event = StubEvent(u"")
        filtered_event = EmailChainFilteredEvent(event)

        filtered_event.content = "One line\n > a email chain line"

        self.assertEqual(
            filtered_event.content,
            "One line\n"
            )

        filtered_event.content = "A second Line\n > another comment\n"

        self.assertEqual(
            filtered_event.content,
            "A second Line\n"
            )


if __name__ == '__main__':
    unittest.main()
