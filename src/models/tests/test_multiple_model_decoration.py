#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
from models.email_chain_filtered_event import EmailChainFilteredEvent
from models.natural_language_filtered_event import NaturalLanguageFilteredEvent
from models.analyzed_event import AnalyzedEvent
from stub_event import StubEvent


class TestEmailChainFilteredEvent(unittest.TestCase):

    def test_natural_language_and_email_chains(self):
        event = StubEvent(u"Some text in English.\n > Some comment\n E algúm texto em português")
        filtered_event = NaturalLanguageFilteredEvent(EmailChainFilteredEvent(event))

        self.assertEqual(
            filtered_event.content,
            "Some text in English.\n"
            )


    def test_chain_all(self):
        event = StubEvent(u"Some text in English.\n > Some comment\n E algúm texto em português")
        filtered_event = AnalyzedEvent(NaturalLanguageFilteredEvent(EmailChainFilteredEvent(event)))

        self.assertEqual(
            filtered_event.content,
            "Some text in English.\n"
            )

        self.assertTrue('pos' in filtered_event.scores)

        self.assertTrue('content' in filtered_event.to_dict())


if __name__ == '__main__':
    unittest.main()
