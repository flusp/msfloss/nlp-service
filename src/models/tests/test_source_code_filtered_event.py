#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
from models.source_code_filtered_event import SourceCodeFilteredEvent
from stub_event import StubEvent


class TestSourceCodeFilteredEvent(unittest.TestCase):

    def test_filter_lines_simple(self):
        event = StubEvent()
        filtered_event = SourceCodeFilteredEvent(event)

        content = u"This is a English Line\n if ( a == 2.3 ) then return a;\nAnother English line"

        self.assertEqual(
            filtered_event.filter_lines(content),
            u"This is a English Line\nAnother English line"
            )

    def test_filter_lines_source_code(self):
        event = StubEvent()
        filtered_event = SourceCodeFilteredEvent(event)

        content = """
        Some English Text

        > if (a != b)  {}
        > else return;

        More English text
        """

        self.assertEqual(
            filtered_event.filter_lines(content),
            "        Some English Text\n        More English text\n        "
            )

    def test_content_decorater(self):
        event = StubEvent(u"This is a English Line\nWhile (x == True); begin; x += 1; end")
        filtered_event = SourceCodeFilteredEvent(event)

        self.assertEqual(
            filtered_event.content,
            u"This is a English Line\n"
            )

    def test_content_setting(self):
        event = StubEvent(u"")
        filtered_event = SourceCodeFilteredEvent(event)

        filtered_event.content = u"This is a English Line\nWhile (x == True); begin; x += 1; end"
        self.assertEqual(
            filtered_event.content,
            u"This is a English Line\n"
            )

        filtered_event.content = u"An English Line\ni++;\n"

        self.assertEqual(
            filtered_event.content,
            u"An English Line\n"
            )


if __name__ == '__main__':
    unittest.main()
