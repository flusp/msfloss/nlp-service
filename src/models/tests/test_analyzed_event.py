#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
from models.analyzed_event import AnalyzedEvent
from models.tests.stub_event import StubEvent
from unittest.mock import MagicMock, Mock

class TestAnalyzedEvent(unittest.TestCase):

    def test_creation_of_scores(self):
        event = StubEvent(content="Some Content")
        analyzed_event = AnalyzedEvent(event)

        self.assertTrue(analyzed_event.content)
        self.assertTrue(analyzed_event.scores)
        self.assertTrue('pos' in analyzed_event.scores)
        self.assertTrue('neg' in analyzed_event.scores)
        self.assertTrue('neu' in analyzed_event.scores)
        self.assertTrue('compound' in analyzed_event.scores)


    def test_injection_of_sentiment_analyzer(self):
        dummy_sentiment_analyzer = Mock()
        dummy_sentiment_analyzer.polarity_scores = MagicMock(return_value={
            'pos': 0.3, 'neg': 0.3, 'neu': 0.4, 'compound': 0.111
        })

        event = StubEvent(content="Some Content")
        analyzed_event = AnalyzedEvent(event,sentiment_analyzer=dummy_sentiment_analyzer)

        self.assertTrue(analyzed_event.scores)

        dummy_sentiment_analyzer.polarity_scores.assert_called_once_with("Some Content")

        self.assertEqual(analyzed_event.scores['pos'],0.3)
        self.assertEqual(analyzed_event.scores['neg'],0.3)
        self.assertEqual(analyzed_event.scores['neu'],0.4)
        self.assertEqual(analyzed_event.scores['compound'],0.111)

        self.assertEqual(dummy_sentiment_analyzer.polarity_scores.call_count,5)


    def test_inability_to_set_scores_manually(self):
        event = StubEvent(content="Some Content")
        analyzed_event = AnalyzedEvent(event)

        def set_scores():
            analyzed_event.scores = {'key':'val'}

        self.assertRaises(AttributeError,set_scores)

    def test_recalculation_of_scores(self):
        event = StubEvent()
        analyzed_event = AnalyzedEvent(event)

        analyzed_event.content = "Some content"
        scores_before = analyzed_event.scores['pos']

        analyzed_event.content = "Some VERY VERY GOOD content"
        scores_after = analyzed_event.scores['pos']

        self.assertGreater(scores_after,scores_before)

    def test_creation_of_dict(self):
        event = StubEvent("Some content")
        analyzed_event = AnalyzedEvent(event)

        dict = analyzed_event.to_dict()

        self.assertTrue('scores' in dict)
        self.assertTrue('pos' in dict['scores'])
        self.assertTrue('neg' in dict['scores'])
        self.assertTrue('neu' in dict['scores'])
        self.assertTrue('compound' in dict['scores'])


if __name__ == '__main__':
    unittest.main()
