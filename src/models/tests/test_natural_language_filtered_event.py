#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
from models.natural_language_filtered_event import NaturalLanguageFilteredEvent
from stub_event import StubEvent


class TestNaturalLanguageFilteredEvent(unittest.TestCase):

    def test_filter_lines_simple(self):
        event = StubEvent()
        filtered_event = NaturalLanguageFilteredEvent(event)

        content = u"This is a English Line\nIsso é uma linha português\nAnother English line"

        self.assertEqual(
            filtered_event.filter_lines(content),
            u"This is a English Line\nAnother English line"
            )

    def test_filter_lines_source_code(self):
        event = StubEvent()
        filtered_event = NaturalLanguageFilteredEvent(event)

        content = """
        Some English Text

        > if (a != b)  {}
        > else return;

        More English text
        """

        self.assertEqual(
            filtered_event.filter_lines(content),
            u"        Some English Text\n        More English text\n"
            )

    def test_content_decorater(self):
        event = StubEvent(u"This is a English Line\nIsso é uma linha português\nAnother English line")
        filtered_event = NaturalLanguageFilteredEvent(event)

        self.assertEqual(
            filtered_event.content,
            u"This is a English Line\nAnother English line"
            )

    def test_content_setting(self):
        event = StubEvent(u"")
        filtered_event = NaturalLanguageFilteredEvent(event)

        filtered_event.content = u"This is a English Line\nIsso é uma linha português\nAnother English line"

        self.assertEqual(
            filtered_event.content,
            u"This is a English Line\nAnother English line"
            )

        filtered_event.content = u"An English Line\nUnd das it deutsch\n"

        self.assertEqual(
            filtered_event.content,
            u"An English Line\n"
            )


if __name__ == '__main__':
    unittest.main()
