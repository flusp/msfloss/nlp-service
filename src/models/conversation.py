#!/usr/bin/env python
# -*- coding: utf-8 -*-

import uuid
from flask import json
from sqlalchemy.dialects.postgresql import UUID, BIGINT, ENUM, DATE
from sqlalchemy import ForeignKey, Column, Table
import sqlalchemy as types
from util.alchemy import db


class Conversation(db.Model):  # Stores Conversations
    __tablename__ = 'conversations'

    id = Column(UUID(as_uuid=True), primary_key=True)
    source = Column(ENUM('IRC', 'ISS', 'MAIL', 'COMM'))
    context = Column(types.JSON())

    def __init__(self, source: str, context: dict):
        self.id = uuid.uuid4()
        self.source = source
        self.context = context

    def __repr__(self):
        return f"<Conversation[{self.id}] source='{self.source}'>"

    def to_dict(self):
        return {
            'id': str(self.id),
            'source': self.source,
            'context': self.context
        }

    def to_response(self):
        return json.dumps(self.to_dict()), 200
