#!/usr/bin/env python
# -*- coding: utf-8 -*-

from models.dynamic_decorator import DynamicDecorator
from langdetect.lang_detect_exception import LangDetectException
import re

class EmailChainFilteredEvent(DynamicDecorator):

    def __init__(self,event):
        super().__init__(event)

    def filter_lines(self,content):
        if content == None:
            return None

        regex = r'^\s*\>+\s+'
        output = []

        for line in content.splitlines(True):
            if not re.match(regex,line):
                output.append(line)

        return ''.join(output)

    @property
    def content(self):
        return self.filter_lines(self._wrappee.content)

    def to_dict(self):
        dict = self._wrappee.to_dict()
        dict['content']  = self.content
        return dict
