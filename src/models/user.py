#!/usr/bin/env python
# -*- coding: utf-8 -*-

import uuid
from flask import json
from sqlalchemy.dialects.postgresql import UUID, BIGINT, ENUM, DATE
from sqlalchemy import ForeignKey, Column, Table
import sqlalchemy as types
from util.alchemy import db

class User(db.Model):  # Stores Users
    __tablename__ = 'users'

    id = Column(UUID(as_uuid=True), primary_key=True)
    name = Column(types.String())

    def __init__(self, name: str):
        self.id = uuid.uuid4()
        self.name = name

    def __repr__(self):
        return f"<User[{self.id}] name='{self.name}'>"

    def to_dict(self):
        return {
            'id': str(self.id),
            'name': self.name
        }

    def to_response(self):
        return json.dumps(self.to_dict()), 200
