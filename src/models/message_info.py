#!/usr/bin/env python
# -*- coding: utf-8 -*-

import uuid
from flask import json
from sqlalchemy.dialects.postgresql import UUID, BIGINT, ENUM, DATE
from sqlalchemy import ForeignKey, Column, Table
import sqlalchemy as types
from util.alchemy import db

class MessageInfo(db.Model):  # Stores event
    __tablename__ = 'message'

    id = Column(types.String, primary_key=True)
    type = Column(types.String())
    message = Column(types.String())
    author = Column(types.String())
    date_start = Column(types.DateTime())
    date_end = Column(types.DateTime())

    def __init__(self, id, type, message, author, date_start, date_end):
        self.id = id
        self.type = type
        self.message = message
        self.author = author
        self.date_start = date_start
        self.date_end = date_end

    def __repr__(self):
        return '<id {}>'.format(self.id)

    def serialize(self):
        return {
            'id': self.id,
            'type': self.type,
            'message': self.message,
            'author': self.author,
            'date_start': self.date_start,
            'date_end': self.date_end
        }
