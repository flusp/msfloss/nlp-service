#!/usr/bin/env python
# -*- coding: utf-8 -*-

from models.dynamic_decorator import DynamicDecorator
from langdetect.lang_detect_exception import LangDetectException
import langdetect

class NaturalLanguageFilteredEvent(DynamicDecorator):

    def __init__(self,event,language='en'):
        super().__init__(event)
        self._language = language

    def filter_lines(self,content):
        output = []

        for line in content.splitlines(True):
            try:
                if langdetect.detect_langs(line)[0].lang == self._language:
                    output.append(line)
            except LangDetectException:
                pass

        return ''.join(output)

    @property
    def content(self):
        return self.filter_lines(self._wrappee.content)

    def to_dict(self):
        dict = self._wrappee.to_dict()
        dict['content']  = self.content
        return dict
