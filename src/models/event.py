#!/usr/bin/env python
# -*- coding: utf-8 -*-

import uuid
from flask import json
from sqlalchemy.dialects.postgresql import UUID, BIGINT, ENUM, TIMESTAMP
from sqlalchemy import ForeignKey, Column, Table
from sqlalchemy.ext.hybrid import hybrid_property
import sqlalchemy as types
from util.alchemy import db, engine
from models.user import User
from sqlalchemy.orm import sessionmaker
from datetime import datetime

class Event(db.Model):  # Stores Events
    __tablename__ = 'events'

    id = Column(BIGINT(), primary_key=True)
    conversation = Column(
        UUID(as_uuid=True), ForeignKey('conversations.id'), primary_key=True)
    _user_id = Column('user',UUID(as_uuid=True), ForeignKey('users.id'))
    timestamp = Column('timestamp',types.DateTime(),default=datetime.now)
    type = Column(ENUM('MSG', 'LIN', 'LOUT'))
    content = Column(types.String())

    def __init__(self,conversation=None,user=None,timestamp=None,type=None,content=None):
        self.conversation = conversation
        self.user = user
        self.timestamp = timestamp
        self.type = type
        self.content = content

    @hybrid_property
    def user(self):
        user =  db.session.query(User).filter_by(id=self._user_id).first()
        if user:
            return user.name
        else:
            return None

    @user.setter
    def user(self,value):
        Session = sessionmaker(bind=engine)
        user = db.session.query(User).filter_by(name=value).first()
        if not user:
            user = User(name=value)
            session = Session()
            session.add(user)
            session.commit()
        self._user_id = user.id

    def __repr__(self):
        return f"<Event:{self.type}[{self.id}] user='{self.user}' '{self.content}'>"

    def to_dict(self):
        return {
            'id': self.id,
            'conversation': str(self.conversation),
            'user': str(self.user),
            'timestamp': datetime.strftime(self.timestamp,"%Y-%m-%dT%H:%M:%S"),
            'type': self.type,
            'content': self.content
        }

    def to_response(self):
        return json.dumps(self.to_dict()), 200

# Define some triggers for the Event Class

@db.event.listens_for(Event, 'before_insert')
def auto_increment_event_id(mapper, connect, target):
    if not target.id:
        last_event_query = db.session.query(Event).filter_by(conversation=target.conversation).order_by(Event.id.desc())
        if last_event_query.count() == 0:
            target.id = 1
        else:
            target.id = last_event_query.first().id + 1
