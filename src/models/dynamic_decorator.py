#!/usr/bin/env python
# -*- coding: utf-8 -*-

class DynamicDecorator(object):

    def __init__(self,wrappee):
        self._wrappee = wrappee

    def __getattr__(self,name):
        if name in [p for p in dir(self._wrappee) if not p.startswith("_")] :
            return self._wrappee.__getattribute__(name)
        else:
            super().__getattribute__(name)

    def __setattr__(self,name,value):
        if name == '_wrappee':
            super().__setattr__(name,value)
        elif name in [p for p in dir(self._wrappee) if not p.startswith("_")] :
            self._wrappee.__setattr__(name,value)
        else:
            super().__setattr__(name,value)
