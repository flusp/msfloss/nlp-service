#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask, jsonify
from flask_restplus import Resource, Api


app = Flask(__name__)
api = Api(app)

api.title = "NLP Service"
api.version = "0.1.0"

app.config.from_object("util.config.Config")
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
