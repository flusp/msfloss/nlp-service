#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask_sqlalchemy import SQLAlchemy
from flask_app import app

import os
from sqlalchemy import create_engine

db = SQLAlchemy(app)
engine = create_engine(os.environ.get('DB_URI'))
