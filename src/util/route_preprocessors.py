#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask_app import app
from util.alchemy import db, engine
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.exc import DataError
from flask_restplus.errors import abort
from models.conversation import Conversation

import re


UUID_PATTERN = re.compile(r'^[\da-f]{8}-([\da-f]{4}-){3}[\da-f]{12}$', re.IGNORECASE)

@app.url_value_preprocessor
def conversation_id_parameter(endpoint, values):

    if values is None or 'conversation_id' not in values:
        return

    conversation_id = values['conversation_id']

    if not UUID_PATTERN.match(conversation_id):
        abort(400,'Invalid Route', errors = {'conversation_id' : "Invalid uuid format for conversation id '%s'"%(conversation_id)})

    try:
        result = db.session.query(Conversation).filter_by(
            id=conversation_id).one()
    except NoResultFound as e:
        abort(404,'Invalid Route', errors = {'conversation_id' : "Could not find any conversation with id '%s'"%(conversation_id)})
