from sqlalchemy.dialects.postgresql import UUID, BIGINT, ENUM
from sqlalchemy import Table, Column, MetaData, ForeignKey
import sqlalchemy as types
from util.alchemy import db, engine
import os

metadata = MetaData(engine)

# Conversations Table
# Create a table with the appropriate Columns
Table("conversations", metadata,
      Column('id', UUID(as_uuid=True),
             primary_key=True, nullable=False),
      Column('source', types.String(), nullable=False),
      Column('context', types.JSON()))

# Users Table
# Create a table with the appropriate Columns
Table("users", metadata,
      Column('id', UUID(as_uuid=True),
             primary_key=True, nullable=False),
      Column('name', types.String(), nullable=False))

# Events Table
# Create a table with the appropriate Columns
Table("events", metadata,
      Column('id', BIGINT(), primary_key=True, nullable=False),
      Column('conversation',
             UUID(as_uuid=True), ForeignKey('conversations.id'), primary_key=True, nullable=False),
      Column('user', UUID(as_uuid=True), ForeignKey('users.id')),
      Column('timestamp', types.DateTime(), nullable=False),
      Column('type', types.String(), nullable=False),
      Column('content', types.String()))

# Implement the creation
metadata.create_all(engine)
