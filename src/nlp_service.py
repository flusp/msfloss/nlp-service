#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from flask_app import app, api
from controllers import conversation_controller
from controllers import event_controller
from controllers import user_controller
from controllers import data_controller
from util import route_preprocessors
from util import setup

if __name__ == "__main__":
    # Start app if run with 'python3 src/nlp_service.py'
    app.run(host='0.0.0.0', port=os.environ.get('PORT', 5000))
